/**
 * Cette classe permet de créer des valeurs personnalisable dans l'execution du code
 */

export class Const {

  public static readonly SPAWN_RATE = 3000; // Frequence d'apparition en milliseconde
  public static readonly SPAWN_TERM = 10000; // Durée d'appartion du pokémon en milliseconde
  public static readonly SPAWN_RADIUS = 1; // Rayon de span en kilomètre
  
}