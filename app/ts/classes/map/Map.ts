import * as mapboxgl from 'mapbox-gl';
import CONFIG        from '../../CONFIG';
import { Position }  from './Position';

class Map {

    public map: mapboxgl.Map;
    
    public DOMmarker: HTMLElement;
    protected popup: mapboxgl.Popup;
    public player_position: Position;
    public DOMpopup: HTMLElement;

    public DOMmapCanvas: HTMLCanvasElement;
    public DOMmapDiv: HTMLElement;
    public webgl_ext;

    constructor() {

        this.map = null;
        this.initMap();

        this.DOMmarker = null;
        this.popup = new mapboxgl.Popup();
        this.DOMpopup = null;

        this.DOMmapCanvas = <HTMLCanvasElement>document.querySelector('.mapboxgl-canvas');
        this.DOMmapDiv = document.getElementById('map');
        this.webgl_ext = this.DOMmapCanvas.getContext('webgl').getExtension('WEBGL_lose_context');

        this.loadMap();

    }

    loadMap() {

        this.map.on( 'load', () => {

            console.log( 'Chargement de la carte.' );
            
            this.DOMmapDiv.style.width = '100%';
            this.DOMmapDiv.style.height = '100%';

            this.map.resize();

            this.DOMmapCanvas.addEventListener("webglcontextlost", (e) => {
                e.preventDefault();
                this.webgl_ext.restoreContext(); }, false);

            this.DOMmapCanvas.addEventListener("webglcontextrestored",
                (e) => e.preventDefault(), false);
        });

    }

    initMap() {

        (mapboxgl as any).accessToken = CONFIG.ACCESSTOKEN;

        this.map = new mapboxgl.Map({
            container: CONFIG.CONTAINER,
            style: CONFIG.STYLE,
            minZoom: 17,
            maxZoom: 20,
            attributionControl: false,
            center: [2.755594, 42.772537], // starting position
        });

    }
}

export default new Map;