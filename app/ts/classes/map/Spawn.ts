import * as mapboxgl    from 'mapbox-gl';
import map              from './Map';
import pokemonService   from '../../services/PokemonService';

import { Tools }          from "../tools/Tools";
import { Player }         from '../models/Player';
import { Const }          from '../tools/Const';
import { Pokemon_info }   from '../section_info/Pokemon_info';
import { Fight }          from '../game/Fight';
import { Pokemon }        from '../models/Pokemon';
import { Position }       from './Position';

/**
 * Cette classe renvoi à l'affichage des pokémons sur la carte ( marqueurs personnalisés )
 */

export class Spawn {
    
    private filter       : number;

    protected player     : Player;
    protected pokemon    : Pokemon;
    
    protected popup      : mapboxgl.Popup;
    protected layer      : mapboxgl.Marker;

    public DOMpopup      : HTMLDivElement;
    public DOMbtn_capture: HTMLButtonElement;
    public DOMbtn_details: HTMLButtonElement;
    public DOMmarker     : HTMLDivElement;

    constructor( player: Player, filter: number = null ) {
        
        this.filter = filter;
        
        this.player  = player;
        this.pokemon = null;
        
        
        this.popup = new mapboxgl.Popup();
        this.layer = null;

        this.DOMpopup       = null;
        this.DOMbtn_capture = null;
        this.DOMbtn_details = null;
        this.DOMmarker      = null;

        this.randomPokemon();
        
    }

    randomPokemon(): void { // Détermine un pokémon aléatoire

        if( !this.filter ) { // sans filtre particulier

            let id = Tools.getRandomIntFloor( 1, 449 ); // Je choisi un pokémon au hasard

            // je récupère les données de ce pokemon.
            pokemonService.detail( id )
                .then( pokemon => {

                    this.pokemon = pokemon;
                    this.render();
                    
                });

        }
        
        else { // avec un filtre (event)

            // je récupère tout les pokémons pour ce filtre
            pokemonService.allForAType( this.filter )
                .then( pokemons_id_pokedex => {

                    let pokemons: Array<Pokemon> = []

                    // j'instanicie seulement les pokémons selon les id récupéré
                    for ( let pokemons_id of pokemons_id_pokedex ) {

                        pokemonService.detail( pokemons_id )
                            .then( pokemon => pokemons.push( pokemon ));

                    }

                    // je laisse le temps au tableau de se remplir
                        setTimeout( () => {
                            
                            let id = Tools.getRandomIntFloor( 0, pokemons.length - 1 )
        
                            pokemonService.detail( pokemons[id].getId() )
                                .then( pokemon => {
                
                                    this.pokemon = pokemon;
                                    this.render();
                                    
                                });

                        }, 200);

            });
        }
    }

    render(): void  { // Apparition des marqueurs personnalisés
        
        // console.log( 'Tentative de spawn' )

        if( this.checkDistance() ) {

            console.log( 'Un spawn a eu lieu !' );

            this.createPopup();
            this.addMarker();

        }
    }

    checkDistance(): boolean { // Condition de distance pour l'apparition

        let playet_lat = this.player.getCoord_player().lat;
        let playet_lng = this.player.getCoord_player().lng;

        this.pokemon.setPosition(
            new Position(
                Tools.getRandomInt(
                    playet_lat - 0.01,
                    playet_lat + 0.01
                ),
                Tools.getRandomInt(
                    playet_lng - 0.01,
                    playet_lng + 0.01
                ),
            )
        );

        // console.log(this.player.coord_player.lat);
        // console.log(this.pokemon.getPosition().lat);

        // je limite la distance entre les coordonnée du joueur et l'apparition des pokémons
        return Tools.distanceInKmBetweenEarthCoordinates(
            playet_lat,
            playet_lng,
            this.pokemon.getPosition().lat,
            this.pokemon.getPosition().lng
        ) < Const.SPAWN_RADIUS ; //SPAWN_RADIUS est la constante configurée dans la classe Const
        
    }

    createPopup(): void { // Parametrage du popup mabbox

        this.DOMpopup = document.createElement('div');
        this.DOMpopup.innerHTML = this.pokemon.getName();
        
        this.DOMbtn_capture = document.createElement('button');
        this.DOMbtn_capture.textContent = 'Capturer';

        this.DOMbtn_capture.addEventListener( 'click', () => {
            
            const fight = new Fight(
                this.player,
                this.player.pokedex.pokemons[
                    Tools.getRandomIntFloor(
                        0,
                        this.player.pokedex.pokemons.length
                    )
                ],
                this.pokemon
            );

            fight.begin();

        });

        this.DOMbtn_details = document.createElement('button');
        this.DOMbtn_details.textContent = 'Détails';
        
        this.DOMbtn_details.addEventListener( 'click', () => {

            const pokemon_info = new Pokemon_info( this.pokemon );
            pokemon_info.dpEventInfo();

        });

        this.DOMpopup.append( this.DOMbtn_capture, this.DOMbtn_details );

        this.popup.setDOMContent( this.DOMpopup );

    }

    addMarker(): void  { // Affiche le marqueur sur la carte

        let decalX = Tools.getDecalX( this.pokemon.getIndex() );
        let decalY = Tools.getDecalY( this.pokemon.getIndex() );

        this.DOMmarker = document.createElement('div');
        this.DOMmarker.style.backgroundPositionX = `-${decalX}px`;
        this.DOMmarker.style.backgroundPositionY = `-${decalY}px`;
        this.DOMmarker.className = `marker`;

        this.layer = (new mapboxgl.Marker( this.DOMmarker ))
            .setLngLat( this.pokemon.getPosition() )
            .setPopup( this.popup )
            .addTo( map.map );

        setTimeout( () => {

            this.layer.remove();
            console.log('Disparition de : ' + this.pokemon.getName() );

        }, Const.SPAWN_TERM );

    }
}