export class Position {

    public lat: number;
    public lng: number;
    
    constructor( lat: number, lng: number ) {

        this.lat = lat;
        this.lng = lng;

    }

    // check() {
    //     return this.lat < 90
    //         && this.lat > -90
    //         && this.lng < 180
    //         && this.lng > -180;
    // }
}