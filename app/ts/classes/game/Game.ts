import menu          from "../section_info/Menu";
import event_creator from "../section_info/Event_creator";

import playerService from "../../services/PlayerService";
import eventService  from "../../services/EventService";

import { Event }  from "../models/Event";
import { Player } from "../models/Player";

class Game {

    public events             : Array<Event>;
    public players            : Array<Player>;

    public DOMform_new_player : HTMLFormElement;
    public DOMinp_name        : HTMLInputElement;

    public DOMhello           : HTMLDivElement;
    public DOMplayer          : HTMLDivElement;

    public DOMmenu            : HTMLDivElement;
    public DOMmenu_list       : HTMLDivElement;

    public DOMdiv_event       : HTMLDivElement;
    public DOMevent           : HTMLDivElement;
    public DOMlist            : HTMLDivElement;

    public isDisplay          : boolean;

    constructor() {

        this.events             = [];
        this.players            = [];

        this.DOMform_new_player = <HTMLFormElement>document.getElementById('form-new-player');
        this.DOMinp_name        = <HTMLInputElement>document.getElementById('name');

        this.DOMhello           = <HTMLDivElement>document.getElementById('hello');
        this.DOMplayer          = null;

        this.DOMmenu            = <HTMLDivElement>document.getElementById('menu');
        this.DOMmenu_list       = <HTMLDivElement>document.getElementById('menu-list');

        this.DOMevent           = <HTMLDivElement>document.getElementById('event');
        this.DOMdiv_event       = <HTMLDivElement>document.getElementById('event-info');
        this.DOMlist            = <HTMLDivElement>document.getElementById('event-info-list');

        this.isDisplay          = false;

        this.addListeners(); // A l'initialisation du jeu, ajouter les écouteurs sur les éléments visible qui ne sont pas propre au joueur.

    }

    initGame(): void {

        playerService.all()
            .then( players => {
                for ( let player of players )
                    this.dpPlayer( player );
            });

        console.log( 'Joueurs dans l\'application' );
        console.log( this.players );

        eventService.all()
            .then( events => {

                for ( let event of events )
                    this.addEvent( event );
                    
            });

        console.log( 'Evénements dans l\'application' );
        console.log( this.events );

    }

    dpPlayer( player: Player ): void {
        
        this.DOMplayer = document.createElement( 'div' );
        this.DOMplayer.className = 'player';
        this.DOMplayer.setAttribute( 'href', `${player.getId()}`)
        this.DOMplayer.innerHTML = player.getName();

        this.addPlayer( player )

        this.DOMplayer.addEventListener( 'click', () => {

            /**
             * A la selection du joueur, je fais disparait l'écran de démarrage pour
             * faire apparaitre la carte.
             * Pour ce joueur, je récupère son index dans l'application et démarre
             * le jeu avec ce joueur
             */

            this.DOMhello.style.display = 'none';
            
            let index_new_player: number = parseInt( this.DOMplayer.getAttribute('href') ) - 1 ;

            this.players[index_new_player].startSession();

        })

        this.DOMhello.append( this.DOMplayer );

    }

    addListeners(): void {

        this.DOMform_new_player.addEventListener( 'submit', (e) =>{

            e.preventDefault();
            
            if( this.DOMinp_name.value )
                this.dpPlayer( new Player( this.DOMinp_name.value ) );

        });

        menu.addListener();
        event_creator.addListener();

    }

    addEvent( event: Event ): void {

        this.events.push( event );
        
    }

    addPlayer( player: Player ): void {
        
        this.players.push( player );
        //TODO fetch playerservice
        
    }

    rmEvent( key: number ): void {
        
        this.events.splice( key, 1 );
        //TODO fetch eventservice

    }

    rmPlayer( key: number ): void {
        
        this.players.splice( key, 1 );
        //TODO fetch playerservice

    }
}

export default new Game;