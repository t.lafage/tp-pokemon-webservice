import pokemonService     from "../../services/PokemonService";

import { Tools }          from "../tools/Tools";
import { Player }         from "../models/Player";
import { Pokemon }        from "../models/Pokemon";


export class Fight {

    private fight      : boolean; //Est-ce que le combat est en cours ?
    private action_time: boolean; //Est-ce qu'une action est en cours ?
    public is_capture  : boolean; //Le pokémon a t il été capturé ?

    public DOMfight  : HTMLDivElement;
    public DOMattack : HTMLDivElement;
    public DOMdefense: HTMLDivElement;
    public DOMaction : HTMLDivElement;

    private player         : Player;
    private pokemon_attack : Pokemon; //pokémon qui attaque ( lancé par le joueur )
    private pokemon_defense: Pokemon; //pokémon qui défend ( le pokémon que le joueur veut récupérer )

    private health_attack : number;
    private health_defense: number;

    private health_attack_decalX: number;
    private health_attack_decalY: number;

    private health_defense_decalX: number;
    private health_defense_decalY: number;


    constructor(
        player          : Player,
        pokemon_attack  : Pokemon,
        pokemon_defense : Pokemon
    ){

        this.fight       = true;
        this.action_time = false;
        this.is_capture  = false;

        this.DOMfight    = <HTMLDivElement>document.getElementById('fight-container');
        this.DOMattack   = <HTMLDivElement>document.getElementById('pokemon-attack-image');
        this.DOMdefense  = <HTMLDivElement>document.getElementById('pokemon-defense-image');
        this.DOMaction   = <HTMLDivElement>document.getElementById('pokemon-fight-action');

        this.player          = player;
        this.pokemon_attack  = pokemon_attack;
        this.pokemon_defense = pokemon_defense;

        this.health_attack   = pokemon_attack.getHealth();
        this.health_defense  = pokemon_defense.getHealth();

        this.health_attack_decalX   = Tools.getBackDecalX( pokemon_attack.getIndex() );
        this.health_attack_decalY   = Tools.getBackDecalY( pokemon_attack.getIndex() );

        this.health_defense_decalX   = Tools.getDecalX( pokemon_attack.getIndex() );
        this.health_defense_decalY   = Tools.getDecalY( pokemon_attack.getIndex() );

    }
    
    display(): void {

        this.DOMfight.style.display = 'block';

        console.log(this.DOMattack );
        console.log( this.health_attack_decalX /64 );
        console.log( this.health_attack_decalX /64);

        this.DOMattack.style.backgroundPositionX  =  `-${this.health_attack_decalX}px`;
        this.DOMattack.style.backgroundPositionY  =  `-${this.health_attack_decalY}px`;

        this.DOMdefense.style.backgroundPositionX  =  `-${this.health_defense_decalX}px`;
        this.DOMdefense.style.backgroundPositionY  =  `-${this.health_defense_decalY}px`;
    }

    begin(): void {

        this.display();

        console.log( 'Attague : ' + this.pokemon_attack.getName() + ' ; ' + this.health_attack );
        console.log( 'Défense : ' + this.pokemon_defense.getName() + ' ; ' + this.health_defense );

        this.DOMaction.addEventListener( 'click', () => {
            if( this.fight && !this.action_time ) {

                this.action_time = true;

                this.health_defense = this.health_defense - this.hit( 10 );

                console.log('vie de la défense : ' + this.health_defense );

                if( this.health_defense <= 0 ) {

                    this.is_capture = true;
                    this.DOMfight.style.display = 'none';
                    this.fight = null;
                    
                    console.log( 'Réussite de la capture');

                    this.capture();

                }

                if( this.fight) {
                    setTimeout( () => {

                        this.health_attack = this.health_attack - this.hit( 5 );

                        console.log('vie de l\'attaque : ' + this.health_attack );

                        if( this.health_attack <= 0 ) {

                            this.is_capture = false;
                            this.DOMfight.style.display = 'none';
                            this.fight = null;

                            console.log( 'échec de la capture.');

                        }

                        this.action_time = false;

                    }, 1500);
                }
            }
        });
    }

    hit( resistance: number ): number {

        let dice = Math.floor( Tools.getRandomInt( 5, 6 ) * resistance );
        let dodge = Tools.getRandomIntFloor( 1, 6 ) == 6;

        return ( !dodge ) ? dice : 0;

    }

    capture() {
        pokemonService.addForAPlayer(
            this.player.getId(),
            this.pokemon_defense.getId()
        )
            .then( message => {

                console.log(message);
                
                if( message == "Savig pokemon success" ) {
                    this.player.pokedex.addPokemon( this.pokemon_defense );
                }

                this.player.pokedex.actualize();
            });
    }
}