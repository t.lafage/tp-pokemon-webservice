import game             from '../game/Game';
import eventService     from '../../services/EventService';
import pokemonService   from "../../services/PokemonService";
import { Section_info } from "./Section_info";

class Event_creator extends Section_info {

    protected classname = 'Event_creator';
    public isDisplay    = false;
    protected DOMinfo   = <HTMLDivElement>document.getElementById('envent-creator');
    protected DOMclose  = <HTMLDivElement>document.getElementById('event-creator-close')

    private DOMmenu_event_creator: HTMLDivElement;
    private DOMform_event_creator: HTMLDivElement;

    public DOMinp_date_start: HTMLInputElement;
    public DOMinp_date_end  : HTMLInputElement;
    private DOMinp_descr    : HTMLInputElement;
    private DOMinp_type     : HTMLSelectElement;
    private DOMerror   : HTMLSpanElement;

    constructor() {

        super();

        this.DOMform_event_creator = <HTMLDivElement>document.getElementById('form-event-creator');

        this.DOMinp_date_start =  <HTMLInputElement>document.getElementById('date-start');
        this.DOMinp_date_end   =  <HTMLInputElement>document.getElementById('date-end');
        this.DOMinp_descr      =  <HTMLInputElement>document.getElementById('description');
        this.DOMinp_type       =  <HTMLSelectElement>document.getElementById('type');
        this.DOMerror          =  <HTMLSpanElement>document.getElementById('error');

    }

    addListener(): void {

        this.createSelectEventType();

        this.DOMform_event_creator.addEventListener( 'submit', (e) => {

            e.preventDefault();
            
            console.log(this.DOMinp_descr.value);
            console.log(this.DOMinp_type.value);

            if(
                this.DOMinp_date_start.value
                && this.DOMinp_date_end.value
                && this.DOMinp_descr.value
                && this.DOMinp_type.value
            ) {

                const form = new FormData;
                form.append( 'date_start' , this.DOMinp_date_start.value );
                form.append( 'date_end'   , this.DOMinp_date_end.value );
                form.append( 'description', this.DOMinp_descr.value );
                form.append( 'type_id'    , this.DOMinp_type.value );
    
                eventService.add( form )
                    .then( event => {

                        if( event )
                            game.addEvent( event );

                    });

            }

            else this.DOMerror.innerText = 'Veuillez remplir tous les champs';
            
        });

        this.DOMclose.addEventListener( 'click', () => {

            this.f_hide()

        });

    }

    createSelectEventType() {

        pokemonService.allTypes()
                .then( types => {

                    for( let type of types ) {
                        
                        let DOMtype_option = document.createElement('option');
                        DOMtype_option.value     = `${type.getId()}`;
                        DOMtype_option.innerText = type.getLabel();

                        this.DOMinp_type.append( DOMtype_option )

                    }

                });

    }

}

export default new Event_creator;