export abstract class Section_info {

    protected abstract classname: string;
    protected abstract isDisplay: boolean;

    protected abstract DOMinfo : HTMLDivElement;
    protected abstract DOMclose: HTMLDivElement;

    dpClassname() {

        console.log(this.classname);

    }

    f_display(): void {

        this.DOMclose.addEventListener( 'click', () => this.f_hide() );

        this.isDisplay = true
        this.DOMinfo.style.display = 'block';

    }

    f_hide() {

        this.isDisplay = false;
        this.DOMinfo.style.display = 'none';

    }
}