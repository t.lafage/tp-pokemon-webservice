import { Pokemon }      from "../models/Pokemon";
import { Player }       from "../models/Player";
import { Tools }        from "../tools/Tools";
import { Pokemon_info }        from "./Pokemon_info";
import { Section_info_player } from "./Section_info_player";


/**
 * Fenêtre du pokedex
 * 
 * Il contient la liste des pokémon du jeu, et découvre ceux possédés
 * par le jouer.
 * Ce n'est pas à proprement parler un inventaire des pokémons possédés,
 * mais un extrait des pokémons du jeu, propre à chaque joueur.
 */

export class Pokedex extends Section_info_player {

    protected classname = 'Pokedex';
    protected isDisplay = false;

    protected DOMinfo  = <HTMLDivElement>document.getElementById('pokedex-info');
    protected DOMclose = <HTMLDivElement>document.getElementById('pokedex-close');
    
    public pokemons     : Array<Pokemon>;

    public DOMpokedex   : HTMLDivElement;
    public DOMinfo_list : HTMLDivElement;

    constructor( player: Player ) {
        
        super( player );

        this.pokemons = [];

        this.DOMpokedex   = <HTMLDivElement>document.getElementById('pokedex');
        this.DOMinfo_list = <HTMLDivElement>document.getElementById('pokedex-info-list');

        this.addListener();

    }

    toJSON(): Object {
        return {
            'pokemons': this.pokemons
        };
    }

    addListener(): void {

        this.DOMpokedex.addEventListener( 'click', () =>
            ( !this.isDisplay ) ? this.display() : this.f_hide() );

    }

    addPokemon( pokemon: Pokemon ): void {

        this.pokemons.push( pokemon );
        
    }

    display(): void { // j'affiche la fenêtre du pokédex

        this.f_display();
        this.actualize();

    }

    actualize(): void {

        this.DOMinfo_list.innerHTML = '';

        for ( let i = 1; i <= 385; i++ ) {

            
            let DOMitem = document.createElement('div');
            DOMitem.className = 'pokedex-info-item';
            DOMitem.nodeValue = `${i}`;
            DOMitem.innerText = `${i}`;

            for ( let pokemon of this.pokemons ) {
                if( pokemon.getIndex() == i ) {

                    DOMitem.innerText = '';

                    let decalX = Tools.getDecalX( pokemon.getIndex() ) + 6;
                    let decalY = Tools.getDecalY( pokemon.getIndex() ) +10;

                    DOMitem.classList.add('have-sprite');

                    DOMitem.style.backgroundPositionX  =  `-${decalX}px`;
                    DOMitem.style.backgroundPositionY  =  `-${decalY}px`;

                    DOMitem.addEventListener( 'click', () => {
                        console.log( pokemon.getId());
                        
                        const pokemon_info = new Pokemon_info( pokemon );
                        pokemon_info.dpEventInfo();
                    } );

                }
            }

            this.DOMinfo_list.append( DOMitem );
        }
    }
}