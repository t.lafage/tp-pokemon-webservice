import game from "../game/Game";
import eventService from "../../services/EventService";

import { Event }  from "../models/Event";
import { Const }  from "../tools/Const";
import { Spawn }  from "../map/Spawn";
import { Player } from "../models/Player";

import { Section_info_player } from "./Section_info_player";
import typeService from "../../services/TypeService";

/**
 * Fenêtre de la liste des événement en cours
 */

export class Event_list extends Section_info_player {

    protected classname = 'Event_list';
    protected isDisplay = false;
    protected DOMinfo   = <HTMLDivElement>document.getElementById('event-info');
    protected DOMclose  = <HTMLDivElement>document.getElementById('event-list-close');

    private DOMevent_list : HTMLDivElement;
    private DOMlist       : HTMLDivElement;
    private DOMend_event  : HTMLDivElement;

    private DOMnew_event  : HTMLSpanElement;
    private DOMstart_event: HTMLButtonElement;

    private current_event: Event;

    constructor( player: Player ) {

        super( player );

        this.DOMevent_list  = <HTMLDivElement>document.getElementById('event');
        this.DOMlist        = <HTMLDivElement>document.getElementById('event-info-list');
        this.DOMend_event   = <HTMLDivElement>document.getElementById('event-end-current');

        this.DOMnew_event   = null;
        this.DOMstart_event = null;

        this.current_event = null;

        this.addListeners();

    }

    addListeners(): void  {

        this.DOMevent_list.addEventListener( 'click', () =>
            ( !this.isDisplay ) ? this.display() : this.f_hide() );

    }

    display(): void {

        this.f_display();
        this.actualize();

    }

    actualize(): void {

        this.DOMlist.innerHTML = '';

        // console.log(game.events[0].date_start);

        if( game.events ) { // si il y a des événement en cours

            for ( let event of game.events ) { // pour chaque événement

                // console.log(event.type_id);
                typeService.find( event.getType_id() )
                    .then( type => {

                        event.setType( type.getLabel() );
                        this.dpStrEvent( event, type );

                    });
            } 
        }

        this.DOMnew_event   = null;
        this.DOMstart_event = null;

    }

    dpStrEvent( event, type ) {

        let str_day_start  : string = event.date_start.getDate().toString();
        let str_month_start: string = ( event.date_start.getMonth() + 1 ).toString();
        let str_day_end    : string = event.date_end.getDate().toString();
        let str_month_end  : string = ( event.date_end.getMonth() + 1 ).toString();

        this.DOMnew_event = document.createElement('p');

        this.DOMnew_event.innerHTML =
            `Du ${str_day_start}/${str_month_start}
            au ${str_day_end}/${str_month_end} :
            élément de type : ${event.getType()}` ;

        /**
         * Exemple pour event = ???:
         * Du ${str_day_start}/${str_month_start}
         * au ${str_day_end}/${str_month_end} :
         * élément de type ${event.type}
         */
        
        this.DOMstart_event = document.createElement('button');
        this.DOMstart_event.value = game.events.indexOf( event ).toString();
        this.DOMstart_event.innerHTML = 'Déclencher cet événement';

        this.DOMstart_event.addEventListener( 'click', (e) => {

            const DOMtarget = e.target as HTMLButtonElement; // DOM du boutton cliqué
            
            clearInterval(this.player.spawn_interval);

            // Lance l'événement dans le cas où aucun événement n'est en cours, sionon le termine
            if( !this.current_event ) this.startEvent( DOMtarget );
            else if( this.current_event ) this.finishEvent( DOMtarget );

        });

        this.DOMlist.append( this.DOMnew_event, this.DOMstart_event );
    }
                

    startEvent( DOMtarget ): void  {
        
        DOMtarget.style.display = 'none';
        this.DOMend_event.style.display = 'block';

        this.current_event = game.events[DOMtarget.value];

        console.log( `Déclanchement de l'événement : ${this.current_event.getId()} ; ${this.current_event.getType()}` );

        // Je redétermine l'apparition des pokémons.
        this.player.spawn_interval = setInterval( () => {
            new Spawn( this.player, this.current_event.getType_id() ) }
        , Const.SPAWN_RATE );

        this.isDisplay = true;
        this.DOMinfo.style.display = 'block';

    }

    finishEvent( DOMtarget ): void  {

        this.DOMend_event.style.display = 'block';
        
        if( this.current_event == game.events[DOMtarget.value] ) {
            
            console.log( 'Fin de l\'événement en cours' + this.current_event.getType_id() );

            let event_id = game.events.indexOf(this.current_event) + 1// Id de l'évenement

            console.log(event_id);
            
            eventService.delete( event_id )
                .then( res => {

                    console.log( res.success );
                    console.log( res.data );

                    if( res.success == true ) {

                        game.rmEvent( event_id - 1 );
                        this.f_hide();
                        
                    }

                });

            this.player.spawn_interval = setInterval( () => {
                new Spawn( this.player ) }, Const.SPAWN_RATE );

            this.current_event = null;
            
        }
    }
};