import { Player } from "../models/Player";
import { Section_info } from "./Section_info";

export abstract class Section_info_player extends Section_info {

    protected player: Player

    constructor( player: Player ) {

        super();

        this.player = player
    }
}