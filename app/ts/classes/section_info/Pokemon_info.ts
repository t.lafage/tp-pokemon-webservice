import { Pokemon }      from "../models/Pokemon";
import { Tools }        from "../tools/Tools";
import { Section_info } from "./Section_info";

export class Pokemon_info extends Section_info {

    protected classname = 'Pokemon_info';
    protected isDisplay = false;
    protected DOMinfo   = <HTMLDivElement>document.getElementById('pokemon-info');

    protected pokemon          : Pokemon;
    
    public DOMinfo_image       : HTMLDivElement;
    public DOMinfo_title       : HTMLDivElement;
    public DOMinfo_description : HTMLDivElement;
    public DOMinfo_categories  : HTMLDivElement;
    public DOMclose            : HTMLDivElement;

    constructor( pokemon: Pokemon ) {

        super();
        
        this.pokemon =  pokemon;

        this.DOMinfo_image       =  <HTMLDivElement>document.getElementById('pokemon-info-image');
        this.DOMinfo_title       =  <HTMLDivElement>document.getElementById('pokemon-info-title');
        this.DOMinfo_description =  <HTMLDivElement>document.getElementById('pokemon-info-description');
        this.DOMinfo_categories  =  <HTMLDivElement>document.getElementById('pokemon-info-categories');
        this.DOMclose            =  <HTMLDivElement>document.getElementById('pokemon-close');

    }

    dpEventInfo(): void {

        let decalX = Tools.getDecalX( this.pokemon.getIndex() );
        let decalY = Tools.getDecalY( this.pokemon.getIndex() );

        this.DOMclose.addEventListener( 'click', () => this.DOMinfo.style.display = 'none' );

        this.DOMinfo.style.display = 'block';

        this.DOMinfo_image.style.backgroundPositionX = `-${decalX}px`;
        this.DOMinfo_image.style.backgroundPositionY = `-${decalY}px`;

        this.DOMinfo_title.innerText       = this.pokemon.getName();
        this.DOMinfo_description.innerText = this.pokemon.getDescription();
        this.DOMinfo_categories.innerText  = this.pokemon.getType_1() + ', ' + this.pokemon.getType_2();

    }

}