import Event_creator    from './Event_creator';
import { Section_info } from './Section_info';

class Menu extends Section_info {

    protected classname = 'Menu';
    protected isDisplay = false;
    protected DOMinfo   = <HTMLDivElement>document.getElementById('menu-info');
    protected DOMclose  = <HTMLDivElement>document.getElementById('menu-close')

    private DOMmenu   : HTMLDivElement;
    private DOMmenu_event_creator   : HTMLDivElement;
    public DOMmenu_list: HTMLDivElement;

    constructor() {

        super();

        this.DOMmenu_event_creator = <HTMLDivElement>document.querySelector('#menu-info > :nth-child(2)');
        this.DOMmenu               = <HTMLDivElement>document.getElementById('menu');

    }

    addListener(): void {

        this.DOMmenu.addEventListener( 'click' , () => {

            if( !this.isDisplay ) 
                this.f_display();

            else if( this.isDisplay )
                this.f_hide() 
                
        });

        this.DOMmenu_event_creator.addEventListener( 'click', () => {

            // Event_creator.actualize();
                
            /**
             * new Date().toISOString().split('T')[0]
             * renvoi la date en format yyyy-mm-dd
             */
            
            Event_creator.DOMinp_date_start.min = new Date().toISOString().split('T')[0];
            Event_creator.DOMinp_date_end.min   = new Date().toISOString().split('T')[0];

            if( !Event_creator.isDisplay )
                Event_creator.f_display();

            else if( Event_creator.isDisplay )
                Event_creator.f_hide();
            
        });

        


        this.DOMclose.addEventListener( 'click', () => {

            this.f_hide();

        });

    }

}

export default new Menu;