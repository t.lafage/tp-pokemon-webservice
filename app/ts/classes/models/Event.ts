import { Model } from "./Model";
import typeService from "../../services/TypeService";

export class Event extends Model {

    private date_start : Date;
    private date_end   : Date;
    private description: string;
    private type_id    : number;

    private type: string;

    getDate_start() {
        return this.date_start;
    }

    getDate_end() {
        return this.date_end;
    }

    getDescription() {
        return this.description;
    }

    getType_id() {
        return this.type_id;
    }

    getType() {
        return this.type;
    }

    setType( type: string ): void {
        this.type = type;
    }


    constructor( date_start: Date, date_end: Date, description: string, type_id: number ) {

        super();
        
        this.date_start  = date_start;
        this.date_end    = date_end;
        this.description = description;
        this.type_id     = type_id;
        
    }
}