import { Model } from './Model';
import { Position } from '../map/Position';

export class Pokemon extends Model {

    private index        : number;
    private name         : string;
    private description  : string;
    private type_1       : string;
    private type_2       : string;
    private PC           : number;
    private health       : number;

    private position     : Position;

    getIndex() {
        return this.index;
    }

    getName() {
        return this.name;
    }

    getDescription() {
        return this.description;
    }

    getType_1() {
        return this.type_1;
    }

    getType_2() {
        return this.type_2;
    }

    getPC() {
        return this.PC;
    }

    getHealth() {
        return this.health;
    }

    getPosition() {
        return this.position;
    }

    setPosition( position: Position ): void {
        this.position = position;
    }
  
    constructor(
        index        : number,
        name         : string,
        description  : string,
        type_1       : string,
        type_2       : string,
        PC           : number,
        health       : number
    ){

        super();

        this.index = index;
        this.name = name;
        this.description = description;
        this.type_1 = type_1;
        this.type_2 = type_2;
        this.PC = PC;
        this.health = health;
    }
}