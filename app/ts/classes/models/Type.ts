import { Model } from './Model';

export class Type extends Model {

    private label: string;

    getLabel() {
        return this.label;
    }
  
    constructor( label: string ) {

        super();

        this.label = label;

    }
}