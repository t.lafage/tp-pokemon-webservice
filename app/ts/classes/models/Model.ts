export abstract class Model {

    protected id: number;

    getId() {
        return this.id;
    }

    setId( id: number ): void {
        this.id = id;
    }

}

export interface ModelLiteral {

    id: number;

}