import * as mapboxgl  from 'mapbox-gl';
import map            from '../map/Map';
import pokemonService from "../../services/PokemonService";

import { Position }       from "../map/Position";
import { Spawn }          from "../map/Spawn";
import { Const }          from '../tools/Const';
import { Pokedex }        from "../section_info/Pokedex";
import { Event_list }     from "../section_info/Event_list";
import { Model }          from "./Model";

/**
 * Cette classe permet d'enregistrer les informations du joueur que l'on enverra au jeu.
 */

export class Player extends Model {

    private coord_player  : Position;
    private name          : string;

    public pokedex        : Pokedex;
    private event_list    : Event_list;

    private DOMmarker    : HTMLElement;

    public spawn_interval: number;

    getCoord_player() {
        return this.coord_player;
    }

    getName() {
        return this.name;
    }

    // getPokedex() {
    //     return this.pokedex;
    // }

    getEvent_list() {
        return this.event_list;
    }

    setPokedex( pokedex) {
        this.pokedex = pokedex;
    }

    constructor( name: string ) {

        super();
        
        this.coord_player = null;
        this.name = name;

        this.pokedex = null;
        this.event_list = null;

        this.DOMmarker = null;

        this.spawn_interval = null;
        
    }

    getPokedex(): void {

        // j'instancie un pokedex
        this.pokedex = new Pokedex( this );

        // que je remplis avec les données du serveur
        pokemonService.allForAPlayer( this.id )
            .then( pokemons_id_pokedex => {
                for ( let pokemons_id of pokemons_id_pokedex ) {

                    pokemonService.detail( pokemons_id )
                        .then( pokemon => this.pokedex.addPokemon( pokemon ) );

                }
            });

        console.log( 'Pokémons dans le pokédex de ' + this.name );
        console.log( this.pokedex.pokemons );

    }

    startSession(): void {

        // je récèpere le pokedex du joueur
        this.getPokedex();

        // console.log( 'Lancement du jeu.' );
        
        navigator
            .geolocation
            .getCurrentPosition( ( geopos ) => {

                // je récupère la géolocalisation du joueur
                const position = new Position(
                    geopos.coords.latitude,
                    geopos.coords.longitude
                );
        
                // j'initialise la carte selon la géolocalisation du joueur
                map.map
                    .setZoom(19)
                    .setPitch(60)
                    .setCenter( position );
                    // .panTo( position );

                this.coord_player = position;

                this.DOMmarker = document.createElement('div');
                this.DOMmarker.className = `player-marker`;

                // je place le marqueur du joueur selon sa géolocalisation
                new mapboxgl.Marker( this.DOMmarker )
                    .setLngLat( this.coord_player )
                    .setPopup( this.createPopup() )
                    .addTo( map.map );

                this.event_list = new Event_list( this );

                //Je lance l'aparition des pokémons
                new Spawn( this );

                this.spawn_interval = setInterval( () => {
                    
                    new Spawn( this );

                }, Const.SPAWN_RATE );
            });
    }

    createPopup(): mapboxgl.Popup {

        let DOMpopup = document.createElement('div');

        DOMpopup.innerHTML =
            `Votre position<br>
            ${this.coord_player.lat},<br>
            ${this.coord_player.lng}`;

        // DOMpopup.append( this.DOMbtn_capture, this.DOMbtn_details );

        return (new mapboxgl.Popup).setDOMContent( DOMpopup );

    }
}