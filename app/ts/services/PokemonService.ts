import CONFIG from "../CONFIG";
import { ModelLiteral } from "../classes/models/Model";
import { Pokemon } from "../classes/models/Pokemon";
import { Type } from "../classes/models/Type";

interface PokemonLiteral extends ModelLiteral {

    index       : number;
    name        : string;
    description : string;
    type_1      : string;
    type_2      : string;
    PC          : number;
    health      : number;

}

interface TypeLiteral extends ModelLiteral {

    label: string;

}

interface DataLiteral {

    success: boolean;
    data   : PokemonLiteral|Array<PokemonLiteral>|string|Array<TypeLiteral>;
    error  : string;

}

class PokemonService {

    private base_url: string = `${CONFIG.SERVER}/pokemons`;

    detail( id ): Promise<Pokemon> {

        const url = `${CONFIG.SERVER}/pokemon/${id}`;

        return this.fetch( url );
        
    }

    addForAPlayer( player_id: number, pokemon_id: number ): Promise<string> {

        const url = `${this.base_url}/add/${player_id}/${pokemon_id}`;

        return fetch( url )
            .then( (res: Response) => res.json() )
            .then( (res: DataLiteral ) => { return <string>res.data })

    }

    allForAPlayer( player_id ): Promise<Array<number>> {

        const url = `${this.base_url}/${player_id}`;

        return this.fetchAll( url );

    }

    allForAType( type_id ): Promise<Array<number>> {

        const url = `${this.base_url}/type/${type_id}`;

        return this.fetchAll( url );

    }

    allTypes(): Promise<Array<Type>> {

        const url = `${this.base_url}/types`;

        return fetch( url )
            .then( (res: Response) => res.json() )
            .then( (res: DataLiteral ) => {
                
                let datas = <Array<TypeLiteral>>res.data;
                
                const types: Array<Type> = []

                for( let data of datas) {

                    const type = new Type( data.label );
                    type.setId( data.id );
                    types.push( type );

                }
                
                return types;
                
            })

    }

    private fetch( url: string ): Promise<Pokemon> {

        return fetch( url )
            .then( (res: Response) => res.json() )
            .then( (res: DataLiteral ) => {
                
                let data = <PokemonLiteral>res.data

                const pokemon = new Pokemon(
                    data.index,
                    data.name.replace(/ .*/,''),
                    // prend le premier mot du nom du pokémon
                    data.description,
                    data.type_1,
                    data.type_2,
                    data.PC,
                    data.health
                );

                pokemon.setId( data.id );
                return pokemon;
                
            })

    }

    private fetchAll( url: string ): Promise<Array<number>> {

        return fetch( url )
            .then( (res: Response) => res.json() )
            .then( (res: DataLiteral) => {

                let datas = <Array<PokemonLiteral>>res.data;

                // const pokedex: Array<Pokemon> = [];

                const pokemons_id_pokedex: Array<number> = [];

                for( let data of datas) {

                    pokemons_id_pokedex.push( data.id );

                    // const pokemon = new Pokemon(
                    //     data.index,
                    //     data.name.replace(/ .*/,''),
                    //     // prend le premier mot du nom du pokémon
                    //     data.description,
                    //     data.type_1,
                    //     data.type_2,
                    //     data.PC,
                    //     data.health
                    // );

                //     pokemon.setId( data.id )
                //     pokedex.push( pokemon );

                }

                return pokemons_id_pokedex;
                
            })
            
    }
}

export default new PokemonService;