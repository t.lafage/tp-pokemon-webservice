import CONFIG from "../CONFIG";
import { Event }        from "../classes/models/Event";
import { ModelLiteral } from "../classes/models/Model";

interface EventLiteral extends ModelLiteral {

    date_start : string;
    date_end   : string;
    description: string;
    type_id    : number;

}

interface DataLiteral {

    success: boolean;
    data   : EventLiteral|string;
    error  : string;

}

class EventService {

    private base_url: string = `${CONFIG.SERVER}/events`;

    all(): Promise<Array<Event>> {

        const url = this.base_url;

        return this.fetchAll( url );
        
    }

    add( form: FormData ): Promise<Event> {

        const url = `${this.base_url}/add`;

        return fetch( url, {
            method: 'POST',
            body: form
        })
            .then( (res: Response) => res.json() )
            .then( (res: DataLiteral) => {

                if( res.success ) {

                    let data = <EventLiteral>res.data

                    const event = new Event(
                        new Date( data.date_start ),
                        new Date( data.date_end ),
                        data.description,
                        data.type_id
                    )

                    event.setId( data.id );

                    return event;

                }
            });
    }

    delete( id: number ): Promise<DataLiteral> {

        const url = `${this.base_url}/del/${id}`;

        return fetch( url )
            .then( (res: Response) => res.json() )
            .then( (res: DataLiteral) => res );
    }


    private fetch( url: string ): Promise<Event> {

        return fetch( url )
            .then( res => res.json() )
            .then( (data: EventLiteral) => {

                const event = new Event(
                    new Date( data.date_start ),
                    new Date( data.date_end ),
                    data.description,
                    data.type_id
                );

                event.setId( data.id );
                return event;
                
            })

    }

    private fetchAll( url: string ): Promise<Array<Event>> {

        return fetch( url )
            .then( res => res.json() )
            .then( (res) => {

                let datas: Array<EventLiteral> = res.data;

                const events: Array<Event> = [];

                if( datas ) {
                    for( let data of datas) {

                        const event = new Event(
                            new Date( data.date_start ),
                            new Date( data.date_end ),
                            data.description,
                            data.type_id
                        );
                        
                        event.setId( data.id );
                        
                        events.push( event );

                    }
                }
                
                return events;
            })
    }

}

export default new EventService;