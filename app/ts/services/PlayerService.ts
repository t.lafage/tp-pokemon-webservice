import CONFIG from "../CONFIG";
import { Player } from "../classes/models/Player";
import { ModelLiteral } from "../classes/models/Model";

interface PlayerLiteral extends ModelLiteral {

    name : string;

}

class PlayerService {

    private base_url: string = `${CONFIG.SERVER}/players`;

    all(): Promise<Array<Player>> {

        const url = this.base_url;

        return fetch( url )
            .then( res => res.json() )
            .then( (res) => {

                let datas: Array<PlayerLiteral> = res.data;

                const players: Array<Player> = [];

                for( let data of datas) {

                    const player = new Player( data.name );
                    player.setId( data.id )
                    players.push( player );

                }

                return players;
                
            })
        
    }

    // allForACategory( id: number): Promise<Array<Post>> {

    //     const url = `${CONFIG.SERVER}/api/categories/${id}`;

    //     return this.fetchAll( url );
    // }

    // showDetail( id: number): Promise<Post> {

    //     const url = `${this.base_url}/${id}`;

    //     return fetch( url )
    //         .then( res => res.json() )
    //         .then( (data: PostLiteral) => {

    //             const post = new Post(
    //                 data.title,
    //                 data.content,
    //                 data.image,
    //                 data.user_id
    //             );

    //             console.log(data);

    //             if( data.user ) {

    //                 const user = new User( data.user.name, data.user.email )
    //                 user.setId( data.id );

                    
                    
    //                 post.setUser( user );

    //             }

    //             return post;
    //         });

    // }

    // private fetchAll( url: string ): Promise<Array<Post>> {

    //     return fetch( url )
    //         .then( res => res.json() )
    //         .then( (datas: Array<PostLiteral>) => {

    //             const posts: Array<Post> = [];

    //             for( let data of datas ) {

    //                 const post = new Post(
    //                     data.title,
    //                     data.content,
    //                     data.image,
    //                     data.user_id
    //                 );

    //                 post.setId( data.id );

    //                 posts.push( post );

    //             }

    //             return posts;
                
    //         })

    // }

}

export default new PlayerService;