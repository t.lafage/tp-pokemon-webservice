import CONFIG from "../CONFIG";
import { Type }        from "../classes/models/Type";
import { ModelLiteral } from "../classes/models/Model";

interface TypeLiteral extends ModelLiteral {

    label : string;

}

interface DataLiteral {

    success: boolean;
    data   : TypeLiteral|Array<TypeLiteral>|string;
    error  : string;

}

class TypeService {

    private base_url: string = `${CONFIG.SERVER}/types`;

    all(): Promise<Array<Type>> {

        const url = `${CONFIG.SERVER}/pokemons/types`;

        return this.fetchAll( url );
        
    }

    find( id: number ): Promise<Type> {

        const url = `${this.base_url}/${id}`;

        return this.fetch( url );
    }


    private fetch( url: string ): Promise<Type> {

        return fetch( url )
            .then( res => res.json() )
            .then( (res: DataLiteral) => {

                let data = <TypeLiteral>res.data

                let type = new Type( data.label );

                type.setId( data.id );
                return type;
                
            })

    }

    private fetchAll( url: string ): Promise<Array<Type>> {

        return fetch( url )
            .then( res => res.json() )
            .then( (res: DataLiteral ) => {

                let datas = <Array<TypeLiteral>>res.data;

                let types: Array<Type> = [];

                if( datas ) {
                    for( let data of datas) {

                        const type = new Type( data.label );

                        type.setId( data.id )
                        types.push( type );

                    }
                }
                
                return types;
            })
    }

}

export default new TypeService;