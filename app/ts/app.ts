import "../scss/styles";
import map  from "./classes/map/Map";
import game from "./classes/game/Game";
import pokemonService from "../ts/services/PokemonService";

import PlayerService from "./services/PlayerService";

import { Data }           from "./classes/tools/Data";
import { Fight }          from "./classes/game/Fight";
import { Player }         from "./classes/models/Player";
import { Pokemon }        from "./classes/models/Pokemon";
import { Position }       from "./classes/map/Position";
import { Spawn }          from "./classes/map/Spawn";
import { Tools }          from "./classes/tools/Tools";


//---------------------
// console.log(Tools.getGeolocation());

// map.setGeolocation();

// PlayerService.all;
//---------------------

//--------
game.initGame();
//--------

//--------
// var fight = new Fight( new Pokemon, new Pokemon);
// fight.begin();
//--------

//---------------------
// const player = new Player( 'alfred');

// game.init( player );
//---------------------

//---------------------
// const fight = new Fight( new Pokemon, new Pokemon );

// console.log(fight.begin());

// player.startSession();
//---------------------