/**
 * Configuartion des données de base de la carte Mapbox
 */

const CONFIG =  {
    ACCESSTOKEN: "pk.eyJ1IjoiYWxwaW5vc2RyZWFtIiwiYSI6ImNqc3VjYWhzbzF4bHc0NG8wdWxpcmc5b2oifQ.SaRfjFqW7daFY_SrfKMrNQ",
    CONTAINER: "map",
    STYLE: "mapbox://styles/alpinosdream/cjteerzem46r51fquywyr87cd",
    SERVER: 'http://localhost/tp-pokemon-webservice/Server'
}

export default CONFIG;